//getEmp.php
<?php 
	
	//Getting the requested id
	$reference_no = $_GET['reference_no'];
	
	//Importing database
	require_once('dbconfig.php');
	
	//Creating sql query with where clause to get an specific employee
	$sql = "SELECT * FROM cashin_customer WHERE reference_no=$reference_no";
	
	//getting result 
	$r = mysqli_query($conn,$sql);
	
	//pushing result to an array 
	$result = array();
	$row = mysqli_fetch_array($r);
	array_push($result,array(
			"name"=>$row['name'],
			"current_balance"=>$row['current_balance'],
			"payment_mode"=>$row['payment_mode'],
			"bank_ac_name"=>$row['bank_ac_name'],
			"description"=>$row['description']
		));

	//displaying in json format 
	echo json_encode(array('result'=>$result));
	
	mysqli_close($conn); 

	?>