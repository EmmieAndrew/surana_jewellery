<?php

include("../config/dbconfig.php");

// for sale -> `name``customer``pay_mode``sub_total``adjustment``total`
// for sale_invoice -> `sale_id``invoice_number``sale_type(sale/return)`
// for sale_jewellery -> `sale_id``jwellery_type``jwellery_name``qty``weight``fine_weight``weight_with_packet``weight_with_packet`
//customer -> customer_id , "sub_total","adjustment"
$rules = ["customer", "pay_mode", "total", "sale_type"];
$rules_je = array();
$postData = $_POST;
//print_r($postData);die;
//var_dump(array_key_exists("customer",$postData));die;
foreach ($rules as $val) {
    //echo $val;
    if (!array_key_exists($val, $postData)) {
        $res = ["message" => "parameter missing ---> " . $val];
        echo json_encode($res);
        die;
    }
}

$postData['name'] = "name";
$name = $postData['name'];
$customer = $postData['customer'];
$pay_mode = $postData['pay_mode'];
$sub_total = $postData['total'];
$adjustment = 0;
//$sub_total = $postData['sub_total'];
//$adjustment = $postData['adjustment'];
$total = $postData['total'];

$sale_sql = "INSERT INTO sale(name,customer,pay_mode,sub_total,adjustment,total) VALUES('$name','$customer','$pay_mode',$sub_total,$adjustment,$total)";
//echo $sale_sql;
if ($conn->query($sale_sql) === TRUE) {
    $sale_id_sql = "select max(sale_id) as sale_id from sale;";
    $result = $conn->query($sale_id_sql);
    $result = $result->fetch_assoc();
    $sale_id = $result['sale_id'];

    $sale_type = $postData['sale_type'];
    //for sale_invoice -> `sale_id``invoice_number``type(sale/return)`
    $invoice_no = date("Ym") . $sale_id;
    $sale_invoice_sql = "INSERT INTO sale_invoice(sale_id,invoice_number,type) VALUES($sale_id,$invoice_no,'$sale_type');";
    //echo $sale_invoice_sql;
    if ($conn->query($sale_invoice_sql) === TRUE) {

    }

    // for sale_jewellery -> `sale_id``jwellery_type``jwellery_name``qty``weight``fine_weight``weight_with_packet``weight_with_packet,amount`
    $saleData = $postData['saleData'];
    //print_r($saleData);

    /* $vv  = [
      ['jwellery_type' => 1,
      'jwellery_name' => 2,
      'qty' => 1,
      'weight' => .10,
      'amount' => 100
      ],['jwellery_type' => 2,
      'jwellery_name' => 3,
      'qty' => 1,
      'weight' => .10,
      'amount' => 100
      ]
      ]; */


    foreach ($saleData as $key => $val) {
        //print_r($val);//die;
        $jew_type = $val['jwellery_type'];
        $jwellery_name = $val['jwellery_name'];
        $qty = $val['qty'];
        $weight = $val['weight'];
        $amount = $val['amount'];


        $sale_pro_sql = "INSERT INTO sale_jewellery(sale_id,jwellery_type,jwellery_name,qty,weight,amount) VALUES($sale_id,$jew_type,$jwellery_name,$qty,$weight,$amount);";
        $conn->query($sale_pro_sql);
    }
}
$json = array(
    "status" => 200,
    "msg" => "success",
    "sale_id" => $sale_id
);



/* Output header */
header('Content-type: application/json');
echo json_encode($json);
?>
