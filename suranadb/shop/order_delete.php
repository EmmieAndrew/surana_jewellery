<?php

include("../config/dbconfig.php");

$postData = $_GET;

if (!isset($postData['sale_id'])) {
    $json = array(
        "status" => 404,
        "msg" => "failed",
        "result" => "parameter missing"
    );
} else {
    //
    $sale_id = $postData['sale_id'];
    $sql = "DELETE FROM `sale` WHERE sale_id=$sale_id;";
    $result = $conn->query($sql);

    $sql = "DELETE FROM `sale_invoice` WHERE sale_id=$sale_id;";
    $result = $conn->query($sql);

    $sql = "DELETE FROM `sale_jewellery` WHERE sale_id=$sale_id;";
    $result = $conn->query($sql);

    //echo $sql;
    $json = array(
        "status" => 200,
        "msg" => "success",
        "result" => "Deleted successfuly"
    );
}






/* Output header */
header('Content-type: application/json');
echo json_encode($json);
?>
