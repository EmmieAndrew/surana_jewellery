<?php 
	//Importing Database Script 
	require_once('../config/dbconfig.php');
	$id=$_GET['id'];
	//Creating sql query
	$sql = "SELECT * FROM customer WHERE customer_id=$id";
	//getting result 
	$r = mysqli_query($conn,$sql);
	
	//creating a blank array 
	$result = array();
	
	//looping through all the records fetched
	while($row = mysqli_fetch_array($r)){
		
		//Pushing name and id in the blank array created 
		array_push($result,array(
			"customer_id"=>$row['customer_id'],
			"name"=>$row['name'],
                        "father_name"=>$row['father_name'],
                        "jewellery_type"=>$row['jewellery_type']
		));
	}
	
	//Displaying the array in json format 
	echo json_encode(array('result'=>$result));
	
	mysqli_close($conn);