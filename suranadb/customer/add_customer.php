<?php

//include '../../config/dbconfig.php';
include '../config/common.inc.php';

$postData = $_POST;
//`name``father_name``mother_name``dob``email``mobile``whatsapp_no``guarantor``balance_amount``as_on_date_balance_amount``amount_payable``as_on_date_payable_amount``date`
//`description``created_at``address_temp``address_permanent``address_proof_type``image_address_proof_front``image_address_proof_back`
$rules1 = ["name", "father_name", "address_permanent"];
$insertData1 = [];
$insertData2 = [];
$files = $_FILES;
foreach ($rules1 as $rule) { 
    if (!isset($postData[$rule])) {
        $json = ["status" => 400, "message" => "parameter missing-->" . $rule];
        $json = json_encode($json);
        echo $json;
        die;
    }
    $insertData1[$rule] = $postData[$rule];
}

//fetch non mandatory fields
$c_non_manda = ["mother_name","dob","email","mobile","whatsapp_no","guarantor","balance_amount","as_on_date_balance_amount","amount_payable","as_on_date_payable_amount","date","description","address_temp","address_permanent","address_proof_type"];
foreach($c_non_manda as $nonman){
	$insertData1[$nonman] = $postData[$nonman];
}



//print_r($insertData1);die;
$rules2 = ["bank_ac_no", "bank_ifsc", "bank_name"];
$insertData2 = array();
foreach ($rules2 as $rule) {
    /*if (!isset($postData[$rule])) {
        $json = ["status" => 400, "message" => "parameter missing-->" . $rule];
        $json = json_encode($json);
        echo $json;
        die;
    }*/
    $insertData2[$rule] = $postData[$rule];
}



    $front = $postData['image_address_proof_front'];
	if(!empty($front)){
		$ImagePath = "uploads/customer/proof_front/" . time() . ".jpg";
		$ServerURL = IMAGE_PATH . $ImagePath;
		//move_uploaded_file($front['tmp_name'], $ImagePath);
		file_put_contents($ImagePath,base64_decode($front));
		$insertData1['image_address_proof_front'] = $ServerURL;
	}
    
	$back = $postData['image_address_proof_back'];
    if(!empty($back)){
    
		$ImagePath = "uploads/customer/proof_back/" . time() . ".jpg";
		$ServerURL = IMAGE_PATH . $ImagePath;
		//move_uploaded_file($back['tmp_name'], $ImagePath);
		file_put_contents($ImagePath,base64_decode($back));
		$insertData1['image_address_proof_back'] = $ServerURL;
	}

$loan_data = "";
//old_data_entry
if (isset($postData['old_data_entry']) && $postData['old_data_entry'] == "yes") {
    $rule_o = ["loan_data", "balance_amount", "amount_payable", "as_on_date_balance_amount", "as_on_date_payable_amount"];
    foreach ($rule_o as $rule) {
        if (!isset($postData[$rule])) {
            $json = ["status" => 400, "message" => "parameter missing-->" . $rule];
            $json = json_encode($json);
            echo $json;
            die;
        }
        $insertData1[$rule] = $postData[$rule];
    }
    // for old data loans json array
    $loan_data = json_decode($postData['loan_data'], true);
}




global $db_connection;
$insert_id = $db_connection->db_insert("customer", $insertData1);
$insertData2['cust_id'] = $insert_id;

$insert_id1 = $db_connection->db_insert("cust_bank_ac", $insertData2);

//insert loan data
if (!empty($loan_data)) {
    foreach ($loan_data as $dt) {
        $d = $dt;
        $d['cust_id'] = $insert_id;
        $db_connection->db_insert("loan_customer", $d);
    }
}

if ($insert_id) {
    $json = array(
        "status" => 200,
        "msg" => "success"
    );
} else {
    $json = array(
        "status" => 404,
        "msg" => mysqli_error()
    );
}

// }



/* Output header */
header('Content-type: application/json');
echo json_encode($json);
?>
