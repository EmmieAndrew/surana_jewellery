<?php

include("../config/common.inc.php");
include("../../config/dbconfig.php");

$postData = $_POST;
//`purchase_other`
//`account``name``invoice_date``time``invoice_no``pay_type``jewellery_detail``jewellery_type`
//`weight``purity``fine_weight``photo``majdoori``description`
$rules = ['photo','account', 'name', 'invoice_date', 'time', 'invoice_no', 'pay_type', 'jewellery_detail', 'jewellery_type', 'weight', 'purity', 'fine_weight', 'majdoori', 'description'];
foreach ($rules as $rule) {
    if (!isset($postData[$rule])) {
        $json = ["status" => 400, "message" => "parameter missing-->" . $rule];
        $json = json_encode($json);
        echo $json;
        die;
    }
    $insertData1[$rule] = $postData[$rule];
}

global $db_connection;

$photo = $postData['photo'];
$ImagePath = "uploads/customer/proof_back/" . time() . ".jpg";
$ServerURL = IMAGE_PATH . $ImagePath;
//move_uploaded_file($back['tmp_name'], $ImagePath);
file_put_contents($ImagePath,base64_decode($photo));

$insertData1['photo'] = $ServerURL;
$insert_id = $db_connection->db_insert("purchase_other", $insertData1);

if ($insert_id) {
    $dt = $db_connection->get_results("purchase_other", "id=" . $insert_id, "single");
    $json = array(
        "status" => 200,
        "msg" => "success",
        "info" => $dt
    );
} else {
    $json = array(
        "status" => 404,
        "msg" => "failed"
    );
}

//}




/* Output header */
header('Content-type: application/json');
echo json_encode($json);
?>
