<?php
/************************
Author: Vivek Maurya
Last Modified 1-03-2015 
************************/


class db_connection
{
	var $totrec;
	var $recid;
	var $fetch_rec;
	var $m_qry;
	var $query;
	var $row;
		
	private $mySQLi;
	public function __construct() {
		global $mysqli;
		$this->mySQLi = $mysqli;
	}	
	//To close the database
	public function db_close() {    		
    		return $this->mySQLi->close();
  	}

	// To print database error message, number and query

	public function db_error($query, $errno, $error) 
	{ 
	    die('<font color="#000000"><b>' . $errno . ' - ' . $error . '<br/><br/>' . $query . '<br/><br/><small><font color="#ff0000">[TEP STOP]</font></small><br/><br/></b></font>');
	}

 	public function db_insert($table,$data,$condition='', $link = 'db_link') 
 	{		
    		reset($data);

      		$query = 'insert into ' . $table . ' (';
      		while (list($columns, ) = each($data)) 
	  	{
			$query .= $columns . ', ';
		}	
      		$query = substr($query, 0, -2) . ') values (';
      		reset($data);
      		while (list(, $value) = each($data)) {
				$value =$this->mySQLi->real_escape_string($value);				
			switch ((string)$value) {
			  case 'now()':
			    $query .= 'now(), ';
			    break;
			  case 'null':
			    $query .= 'null, ';
			    break;
			  default:
			    $query .= '\'' .$value. '\', ';
			    break;
			}
      		}

      		$query = substr($query, 0, -2) . ')';

      		
    		//echo $query;
		$this->db_query($query);
		$this->recid=$this->mySQLi->insert_id;
    		return $this->recid;
  	}  

  	public function db_update($table,$data,$condition = '', $link = 'db_link')
   	{

 		$query = 'update ' . $table . ' set ';
      		while (list($columns, $value) = each($data)) {
		     	if($columns=='inccredits'){
			  $query .= 'credits'. ' ='.$value.', ';
			  continue;
			}
				$value =$this->mySQLi->real_escape_string($value);
        		switch ((string)$value) {
			  case 'now()':
			    $query .= $columns . ' = now(), ';
			    break;
			  case 'null':
			    $query .= $columns .= ' = null, ';
			    break;
			  default:
			    $query .= $columns . ' = \'' .$value. '\', ';
			    break;
			}

      		}
	   	$query = substr($query, 0, -2) . ' where ' . $condition;
	   	//print_r($query); exit();
	   	$this->db_query($query);
		return $this->m_qry;
	}
	
	public function db_update_debug($table,$data,$condition = '', $link = 'db_link')
   	{
 		$query = 'update ' . $table . ' set ';
      		while (list($columns, $value) = each($data)) {
		     	if($columns=='inccredits'){
			  $query .= 'credits'. ' ='.$value.', ';
			  continue;
			}
				$value =$this->mySQLi->real_escape_string($value);
        		switch ((string)$value) {
			  case 'now()':
			    $query .= $columns . ' = now(), ';
			    break;
			  case 'null':
			    $query .= $columns .= ' = null, ';
			    break;
			  default:
			    $query .= $columns . ' = \'' .$value. '\', ';
			    break;
			}

      		}
	   	echo $query = substr($query, 0, -2) . ' where ' . $condition;
		
	   	
	   	$this->db_query($query);
		return $this->m_qry;
	}
	
   
	public function insert_update($tblname,$arrfield,$action=" insert into ",$wherecondition="")
	{
		if (is_array($arrfield))
		{
			while(list($column,$value) = each($arrfield))
			{
				@$query.= ",".$column."='".$value."'";
			}
		}
		$query1 = substr($query,1);
		$sql = "$action $tblname set $query1 $wherecondition";
		$res = $this->db_query($sql);
		$insertid = $this->mySQLi->insert_id();
		return $insertid;
	}

	//for delete record
	public function db_delete($table,$condition = '')
	{ 
	    	$query='Delete from '. $table .' where '. $condition;
		$this->m_qry = $this->db_query($query);
		return $this->m_qry;
	}

	public function db_query($query)
   	{
		$this->query=$query;		
    		$this->m_qry=$this->mySQLi->query($this->query) or $this->db_error($query,$this->mySQLi->errno,$this->mySQLi->error);
		return $this->m_qry;
   	}

	public function db_num_rows()
	{
		return $this->totrec=$this->result->num_rows($this->m_qry);
	}

	public function db_fetch_assoc() {		
		return $this->result->fetch_assoc();;
	}
	
	public function get_results($tbl_name,$tbl_cols,$whr_cond,$rec_type){   
		
		if($tbl_cols=='*')
		{
			$tbl_cols_names = " * ";
		
		}
		else
		{
			if(count($tbl_cols)>0)
				$tbl_cols_names = implode(',',$tbl_cols);
		}
		$select = "SELECT ".$tbl_cols_names." FROM ".$tbl_name." ".$whr_cond." ".$order_by." ".$limit;
		$this->result = $this->db_query( $select );
		$db_results = array();		
		if ( $this->result ) {	
			if($rec_type=='single'){
				$db_results = $this->db_fetch_assoc($this->result);
			}
			else
			{		
				while($db_row = $this->db_fetch_assoc($this->result)){
						$db_results[] = $db_row;				
				}
			}
		}		
		return $db_results;
	}
	
	public function get_results_limit($tbl_name,$tbl_cols,$whr_cond,$order_by='',$limit='',$rec_type){
	
		if($tbl_cols=='*')
		{
			$tbl_cols_names = " * ";
	
		}
		else
		{
			if(count($tbl_cols)>0)
				$tbl_cols_names = implode(',',$tbl_cols);
		}
		$select = "SELECT ".$tbl_cols_names." FROM ".$tbl_name." ".$whr_cond." ".$order_by." ".$limit;
		$this->result = $this->db_query( $select );
		$db_results = array();
		if ( $this->result ) {
			if($rec_type=='single'){
				$db_results = $this->db_fetch_assoc($this->result);
			}
			else
			{
				while($db_row = $this->db_fetch_assoc($this->result)){
					$db_results[] = $db_row;
				}
			}
		}
		return $db_results;
	}
	
	public function get_results_query($query,$rec_type){  
		
		$select = $query;
		$this->result = $this->db_query( $select );
		$db_results = array();		
		if ( $this->result ) {	
			if($rec_type=='single'){
				$db_results = $this->db_fetch_assoc($this->result);
			}
			else
			{		
				while($db_row = $this->db_fetch_assoc($this->result)){
						$db_results[] = $db_row;				
				}
			}
		}		
		return $db_results;
	}

	public function check_for_duplicate($tbl_name,$tbl_cols,$whr_cond){   
		
		if($tbl_cols=='*')
		{
			$tbl_cols_names = " * ";		
		}
		else
		{
			if(count($tbl_cols)>0)
				$tbl_cols_names = implode(',',$tbl_cols);
		}
		$select = "SELECT ".$tbl_cols_names." FROM ".$tbl_name." ".$whr_cond;
		
		$this->result = $this->db_query( $select );
		$db_results = array();		
		if ( $this->result ) {				
			$db_results = $this->db_fetch_assoc($this->result);
			if($db_results){				
				return $db_results;
			}else{
				return false;
			}
		}	
	}


	public function updatePoints($query){
		$this->result = $this->db_query( $query );
	}
 }
?>
